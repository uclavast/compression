# Accelerator Code Review: Compression

## Reviewers
Young-kyu Choi (ykchoi@cs.ucla.edu)

## Problems

### Young's comment

#### 1. Issue with #pragma HLS dataflow
In my experience with EMTV and working with Kuan, my conclusion is that it is dangerous to use #pragma HLS dataflow when the loop bound is unknown at compile time.

HLS has a habit of restarting a module even if it ends earlier than other modules. This sometimes make the whole design never-ending.

I remember putting the loop bound to a fixed value seems to help get around this problem.

Or getting rid of HLS dataflow and going with double-buffering-like coding style never causes this problem.

#### 2. Sharing in_size_local variable among multiple function

Could you check the generated RTL file if in_size_local variable is being synthesized correctly? Another side effect of HLS dataflow is that it sometimes incorrectly synthesize a shared scalar variable.

Since it is a scalar variable, it should be synthesized as a "wire", but HLS sometimes registers it of which triggering timing can never be satisfiied - resulting in "0" value for that register all the time.

One workaround is to duplicate this variable in the top function - in_size_local1, in_size_local2, in_size_local3, although they are exactly the same variable.


## Suggestions
Other suggestions.


### Jie's comment
In this design, 'stream' is well utilized as an interface between two functions, which helps to realize a pipeline structure in function level.

#Suggestion
'out_size', as one of the arguments associated to the kernel function 'deflate259', is specified as a independent global memory buffer. However, this buffer contains just one element. It is a little bit waste to return a value by using a dedicated memory BUS.
