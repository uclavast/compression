# Compression

## Developer
Libo Wang (wanglibo@ucla.edu)

## Description
The deflate algorithm (https://www.ietf.org/rfc/rfc1951.txt) is a commonly used compression algorithm in various different applications. 

We implement this stall free pipelined version of parallel deflate algorithm on FPGA, inspired by "A Scalable High-Bandwidth Architecture for Lossless Compression on FPGAs"(Fowers et al, FCCM 2015, http://research.microsoft.com/pubs/245093/fccm2015_cr2.pdf), and "Gzip on a Chip: High Performance Lossless Data Compression on FPGAs using OpenCL"(Abdelfattah et al, IWOCL 2014, http://www.eecg.utoronto.ca/~mohamed/iwocl14.pdf). Instead of using RTL and OpenCL, we used HLS to implement this algorithm.

A detailed explanation of our algorithm with figures can be found in algorithm.pdf. It's HIGHLY RECOMMENDED to read that file to get a basic understanding of the pipeline and algorithm, then start reading the code, referring back to the document as needed.

## Design Specification
This design runs at 200 MHz, 16 bytes/cycle; the peak throughput is therefore ~3GBps. We use a hash table of 16 banks, 256x 16-byte entries in each bank (total size 64kB).

## Code Navigation
The resources in src folders are the following:

- constant.h: Design parameters such as cycle throughput, hash table banks and size, input/output double buffer depth, etc.
- deflate_dataflow.cpp: Main steps of the program
- huffman_translate.cpp: Contains the method to perform static Huffman encoding and encoding table.
- reduction.cpp: Contains a method to compute minimum in an array with reduction.
- opencl_tb.c: OpenCL/SDAccel testbench file
- zpipe.c: HLS testbench file
- run_hls.tcl: HLS start script
- host_bin.tcl: SDAccel start script
- host_only.tcl: SDAccel host compilation only script
- run_bin.sh: copy the bitstream into the project created by host only compilation, and run the OpenCL application.

## Test Results

- **Environment Setup**
    - Xilinx SDAccel 2015.1.5
    - AlphaData card
    - symphony.cs.ucla.edu

- **Performance**
    - Compressed 40335752 Bytes in an average of 0.0338s.

- **Throughput**
    - 1.11 GBps execution time including data transfer. Execution time breakdown: 50% data transfer, 33% execution, 17% software driver